#!/vendor/bin/sh
if ! applypatch --check EMMC:/dev/block/by-name/recovery$(getprop ro.boot.slot_suffix):69009408:d802f55739822fe1b2bb66e6569eae62cf52b4fc; then
  applypatch \
          --patch /vendor/recovery-from-boot.p \
          --source EMMC:/dev/block/by-name/boot$(getprop ro.boot.slot_suffix):61865984:21d6eaf68eb46f0c40669c1a46c3b6e782081820 \
          --target EMMC:/dev/block/by-name/recovery$(getprop ro.boot.slot_suffix):69009408:d802f55739822fe1b2bb66e6569eae62cf52b4fc && \
      (/vendor/bin/log -t install_recovery "Installing new recovery image: succeeded" && setprop vendor.ota.recovery.status 200) || \
      (/vendor/bin/log -t install_recovery "Installing new recovery image: failed" && setprop vendor.ota.recovery.status 454)
else
  /vendor/bin/log -t install_recovery "Recovery image already installed" && setprop vendor.ota.recovery.status 200
fi

